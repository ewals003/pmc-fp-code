#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "../apps/myApps/myJubilantSketch/balls.h"
using namespace glm;

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		//ofLight lighting;
		//ofEasyCam cam;
		//ofVboMesh body;
		//ofMaterial mat;

		balls balls_bounce;
		ofVboMesh mesh;
		int texReso;
		float Xstep;
		float Ystep;

		ofEasyCam cam;

		ofxPanel panel;
		ofxIntSlider mode, colour;
		ofxFloatSlider smooth_sharp, a, b, c, d, e, f;
		ofxFloatSlider rotation;
		ofShader twist, base;
		ofLight lighting;



private:

	float point;


		
};
