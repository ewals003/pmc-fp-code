#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup(){
	
	ofSetBackgroundColor(0);
	ofEnableDepthTest();
	ofEnableLighting();
	ofEnableNormalizedTexCoords();
	lighting.setPointLight();
	lighting.setDiffuseColor(ofFloatColor(1.0, 1.0, 1.0));
	lighting.setAmbientColor(ofFloatColor(0.3, 0.3, 0.3));
	lighting.setPosition(ofGetWidth() * .5, ofGetHeight() * .5,500);
	cam.setPosition(ofGetWidth() * .5, ofGetHeight() * .5, 400);


	mat.setDiffuseColor(ofFloatColor(1.0, 1.0, 1.0));
	mat.setSpecularColor(ofFloatColor(0.7, 0.7, 0.7));
	mat.setShininess(50.0f);
	mat.setAmbientColor(ofFloatColor(0.4, 0.4, 0.4));

	
	ofSetVerticalSync(true);
	ofBackground(0);

	cam.setupPerspective();
	cam.setNearClip(10);
	cam.setPosition(0, 0, 300);

	twist.load("../apps/myApps/myJubilantSketch/bin/data/shader/twist.frag");
	base.load("shader/base.vert","shader/base.frag");
	stringstream baseVSrc;
	baseVSrc << "#version 120\n";
	baseVSrc << "uniform sampler2DRect u_positionsTex;\n";
	baseVSrc << "void main(){\n";
	baseVSrc << "vec2 st = gl_MultiTexCoord0.xy;\n";
	baseVSrc << "vec3 pos= texture2DRect(u_positionsTex, st).xyz;\n";
	baseVSrc << "gl_PointSize = 1.0;\n";
	baseVSrc << "gl_Position = gl_ModelViewProjectionMatrix * vec4(pos, 1.0);\n";
	baseVSrc << "gl_FrontColor = gl_Color;\n";
	baseVSrc << "}\n";

	stringstream baseFSrc;
	baseFSrc << "#version 120\n";
	baseFSrc << "void main(){\n";
	baseFSrc << "vec2 st = gl_TexCoord[0].st;\n";
	baseFSrc << "gl_FragColor = gl_Color;\n";
	baseFSrc << "}\n";

	stringstream twistSrc;
	twistSrc << "#version 120\n";
	twistSrc << "#define PI 3.1415926535897932384\n";
	twistSrc << "uniform sampler2DRect u_positionsTex;\n";
	twistSrc << "uniform float Xstep;\n";
	twistSrc << "uniform float Ystep;\n";
	twistSrc << "uniform float smooth_sharp;\n";
	twistSrc << "uniform float a;\n";
	twistSrc << "uniform float b;\n";
	twistSrc << "uniform float c;\n";
	twistSrc << "uniform float d;\n";
	twistSrc << "uniform float e;\n";
	twistSrc << "uniform float f;\n";
	twistSrc << "vec3 sf3d(float x, float y){\n";
	twistSrc << "float p = -PI + Xstep * x;\n";
	twistSrc << "float q = -PI/2.0 +Ystep *y;\n";
	twistSrc << "float rawr1= pow(abs(1.0/a * abs(cos(c*p/4.0))), e)+ pow(abs(1.0/b*abs(sin(c*p/4.0))),f);\n";
	twistSrc << "float n1 = pow(abs(rawr1), -1.0/d);\n";
	twistSrc << "float rawr2= pow(abs(1.0/a * abs(cos(c*q/4.0))), e)+ pow(abs(1.0/b*abs(sin(c*q/4.0))),f);\n";
	twistSrc << "float n2=pow(abs(rawr2), -1.0/d);\n";
	twistSrc << "float ex=n1* cos(p)*n2*cos(q)*100.0;\n";
	twistSrc << "float why=n1*cos(p)*n2*cos(q)*100.0;\n";
	twistSrc << "float zed=n2*sin(q)*100.0;\n";
	twistSrc << "return vec3(ex,why,zed);\n";
	twistSrc << "}\n";
	twistSrc << "void main(){\n";
	twistSrc << "vec2 st = gl_TexCoord[0].st;\n";
	twistSrc << "vec3 curPos = texture2DRect(u_positionsTex,st).xyz;\n";
	twistSrc << "vec3 sPos = sf3d(st.x,st.y);\n";
	twistSrc << "vec3 difference = sPos - curPos;\n";
	twistSrc << "curPos += difference *smooth_sharp/100;\n";
	twistSrc << "gl_FragData[0] = vec4(curPos,1.0);\n";
	twistSrc << "}\n";



	base.setupShaderFromSource(GL_VERTEX_SHADER, baseVSrc.str());
	base.setupShaderFromSource(GL_FRAGMENT_SHADER, baseFSrc.str());
	base.bindDefaults();
	base.linkProgram();
	twist.setupShaderFromSource(GL_FRAGMENT_SHADER, twistSrc.str());
	twist.bindDefaults();
	twist.linkProgram();
	
	int vert = 1000;
	texReso = ceil(sqrt(vert));
	for (int i = 0; i < texReso; i++) {
		for (int j = 0; j < texReso; j++) {
			int index = i * texReso + j;
			mesh.addVertex(ofVec3f(0, 0, 0));
			mesh.addTexCoord(ofVec2f(i, j));
			mesh.addColor(ofFloatColor(1.0, 1.0, 1.0, 1.0));

		}
	}
	vector<int> lastRow;
	for (int x = 0; x < texReso; x++) {
		for (int y = 0; y < texReso; y++) {
			if (x == texReso - 1) {
				int id1 = x * texReso + y;
				int id2 = y * texReso + y + 1;
				int id3 = y + 1;
				int id4 = y;
				mesh.addTriangle(id1, id2, id3);
				mesh.addTriangle(id1, id3, id4);
			}
			else {
				int id1 = x * texReso + y;
				int id2 = y * texReso + y + 1;
				int id3 = (x+1)*texReso+y+1;
				int id4 = (x+1)*texReso+y;
				mesh.addTriangle(id1, id2, id3);
				mesh.addTriangle(id1, id3, id4);
				if(y==texReso-2){
					lastRow.push_back(id2);
				}
			}
			
		}
	}
	int lastVert = mesh.getNumVertices();
	for (int i = 0; i < lastRow.size() - 1; i++) {
		mesh.addTriangle(lastRow[i], lastRow[i + 1], lastVert);
		mesh.addTriangle(0, lastRow[i] + 1, lastRow[i + 1] + 1);

	}
	mesh.addTriangle(lastRow[lastRow.size() - 1], lastRow[0], lastVert);

	
	balls_bounce.allocate(texReso, texReso, GL_RGBA32F, 1);
	float* positions = new float[texReso * texReso * 4];
	for (int x = 0; x < texReso; x++) {
		for (int y = 0; y < texReso; y++) {
			int index = y * texReso + x;
			positions[index * 4 + 0] = 0;
			positions[index * 4 + 1] = 0;
			positions[index * 4 + 2] = 0;
			positions[index * 4 + 3] = 0;
		}
	}
	balls_bounce.src->getTexture(0).loadData(positions, texReso, texReso, GL_RGBA);
	delete[] positions;

	Ystep = 2.0 * PI / texReso;
	Xstep = PI / texReso;

	panel.setup();
	panel.add(mode.setup("mode", 0, 0, 10));
	panel.add(colour.setup("colour", 0, 0, 2));
	panel.add(rotation.setup("smoothing", 0.1, 0, 0.5));
	panel.add(smooth_sharp.setup("smooth or sharpen", 5, 1, 15));
	panel.add(a.setup("a", 1, 0, 5));
	panel.add(b.setup("b", 1, 0, 5));
	panel.add(c.setup("c", 1, 0, 40));
	panel.add(d.setup("d", 1, 0, 5));
	panel.add(e.setup("e", 1, 0, 5));
	panel.add(f.setup("f", 1, 0, 5));

}

//--------------------------------------------------------------
void ofApp::update(){
	balls_bounce.dst->begin();
	twist.begin();
	twist.setUniformTexture("u_positionsTex", balls_bounce.src->getTexture(0),0);
	twist.setUniform1f("X step", Xstep);
	twist.setUniform1f("Y step", Ystep);
	twist.setUniform1f("smoothing", smooth_sharp);
	twist.setUniform1f("a", a);
	twist.setUniform1f("b", b);
	twist.setUniform1f("c", c);
	twist.setUniform1f("d", d);
	twist.setUniform1f("e", e);
	twist.setUniform1f("f", f);
	balls_bounce.src->draw(0, 0);
	twist.end();
	balls_bounce.dst->end();

	balls_bounce.swap();

	switch (mode) {
	case 0:
		mesh.setMode(OF_PRIMITIVE_POINTS);
		break;
	default:
		mesh.setMode(OF_PRIMITIVE_TRIANGLES);
		break;

	}
	switch (colour)
	{
	case 0:
		for (int x = 0; x < texReso; x++) {
			for (int y = 0; y < texReso; y++) {
				int index = y * texReso + x;
				mesh.setColor(index, ofColor(ofMap(abs(y - texReso / 2), 0, texReso / 2, 0, 255), ofMap(abs(x - texReso / 2), 0, texReso / 2, 0, 255),120));



			}
		}
		break;
	default:
		break;
	}
	point += rotation;
	if (point > 360) {
		point -= 360;
	}


}

//--------------------------------------------------------------
void ofApp::draw(){
/*	lighting.enable();
	cam.begin();
	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2,0);
	mat.begin();
	ofDrawSphere(100);
	mat.end();
	cam.end();
	lighting.disable();
	*/
	ofSetWindowTitle(ofToString(ofGetFrameRate()));
	ofPushStyle();
	cam.begin();

	switch (colour) {
	case 0:
		glShadeModel(GL_SMOOTH);
		break;
	case 1:
		glShadeModel(GL_FLAT);
		break;
	default:
		break;
	}
	ofEnableDepthTest();
	ofRotate(point);
	base.begin();
	base.setUniformTexture("u_positionsTex", balls_bounce.src->getTexture(0), 0);
	switch (mode) {
		mesh.draw();
		break;
	case 1:
		mesh.drawFaces();
		break;
	default:
		mesh.drawWireframe();
		break;
	}
	base.end();
	ofDisableDepthTest();
	cam.end();
	ofPopStyle();
	panel.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
