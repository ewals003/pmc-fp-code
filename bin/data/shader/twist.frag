#version 120

#define PI 3.1415926535897932384

uniform sampler2DRect u_positionsTex;
uniform float Xstep;
uniform float Ystep;
uniform float smooth_sharp;
uniform float a;
uniform float b;
uniform float c;
uniform float d;
uniform float e;
uniform float f;

vec3 sf3d(float x, float y){
float p = -PI + Xstep * x;
float q = -PI/2.0 +Ystep *y;

float rawr1= pow(abs(1.0/a * abs(cos(c*p/4.0))), e)+ pow(abs(1.0/b*abs(sin(c*p/4.0))),f);
float n1 = pow(abs(rawr1), -1.0/d);
float rawr2= pow(abs(1.0/a * abs(cos(c*q/4.0))), e)+ pow(abs(1.0/b*abs(sin(c*q/4.0))),f);
float n2=pow(abs(rawr2), -1.0/d);

float ex=n1*cos(p)*n2*cos(q)*100.0;
float why=n1*cos(p)*n2*cos(q)*100.0;
float zed=n2*sin(q)*100.0;
return vec3(ex,why,zed);

}
void main(){
vec2 st = gl_TexCoord[0].st;
vec3 curPos = texture2DRect(u_positionsTex,st).xyz;
vec3 sPos = sf3d(st.x,st.y);

vec3 difference = sPos - curPos;
curPos += difference *smooth_sharp/100;
gl_fragData[0] = vec4(curPos,1.0);
}